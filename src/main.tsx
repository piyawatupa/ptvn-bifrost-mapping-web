import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createBrowserHistory } from "history";
import { configureStore } from "app/store";
import { Router } from "react-router-dom";
import RouterApp from "./app/index";
import "./assets/App.css";
// prepare store
const history = createBrowserHistory();
const store = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <RouterApp
        history={history}
        location={null}
        match={null}
      />
    </Router>
  </Provider>,
  document.getElementById("root")
);
