import styled from 'styled-components';

const Title = styled.h1`
   background: green;

   .ant-advanced-search-form {
      padding: 24px;
      background: #fbfbfb;
      border: 1px solid #d9d9d9;
      border-radius: 6px;
      color: red;
    }
    
    .ant-advanced-search-form .ant-form-item {
      display: flex;
    }
    
    .ant-advanced-search-form .ant-form-item-control-wrapper {
      flex: 1;
    }
`

export {
  Title
}