import * as React from 'react';
//import { Title } from './index.view'
import Layout from 'app/components/Layouts';
import Router from './Router';

export namespace Home {
  export interface HomeProps {}

  export interface HomeState {}
}
class Home extends React.Component {
  constructor(props: Home.HomeProps) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Layout {...this.state}>
        <Router />
      </Layout>
    );
  }
}

export default Home;
