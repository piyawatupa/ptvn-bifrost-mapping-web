import * as React from 'react';
import { Switch, Route } from 'react-router-dom';
import Error404 from '../404';
import { PrivateRoute } from '../../middleware';
import { PageEnum } from 'app/utils/enums';
import Dashboard from '../Dashboard';
import ProxyList from '../Proxy/ProxyList';
import ProjectList from '../Project/ProjectList';
import MappingList from '../Mapping/MappingList';
import JsonBody from '../JsonData/JsonBody';
interface RouterProps {}

const Router: React.FC<RouterProps> = (props: any) => {
  return (
    <Switch>
      <PrivateRoute exact path={PageEnum.HOME} component={Dashboard} />
      <PrivateRoute exact path={PageEnum.PROXY_LIST} component={ProxyList} />
      <PrivateRoute exact path={PageEnum.PROJECT_LIST} component={ProjectList} />
      <PrivateRoute exact path={PageEnum.MAPPING_LIST} component={MappingList} />
      <PrivateRoute exact path={PageEnum.JSONBODY} component={JsonBody} />
      <Route path="*" component={Error404} />
    </Switch>
  );
};

export default Router;
