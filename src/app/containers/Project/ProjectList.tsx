import * as React from 'react'
import { useProject } from '@hook/useProject'
import { Button, Col, Dropdown, Form, Input, Menu, Row, Select, Typography } from 'antd'
import { DownOutlined } from '@ant-design/icons'
import { useState } from 'react'
const { Paragraph } = Typography
const { Option } = Select
import Drawer from '@components/Drawer'
import Table from '@components/Table'

interface ProjectProps {}
const ProjectList: React.FC<ProjectProps> = (props: any) => {
	const { projectList, loading } = useProject()
	let [drawer, setDrawer] = useState({ visible: false, toggle: false })
	const menu = (
		<Menu>
			<Menu.Item key="1">Edit</Menu.Item>
		</Menu>
	)

	const columns = [
		{
			title: 'ID',
			dataIndex: 'id',
			render: (text: any, record: any, index: any) => {
				return <Paragraph copyable={{ text: JSON.stringify(record) }}>{record.id}</Paragraph>
			},
		},
		{
			title: 'Name',
			dataIndex: 'projectName',
		},
		{
			title: 'Description',
			dataIndex: 'description',
		},
		{
			title: 'Action(s)',
			render: () => {
				return (
					<Dropdown overlay={menu}>
						<Button>
							Action(s) <DownOutlined />
						</Button>
					</Dropdown>
				)
			},
		},
	]

	const handleDrawerToggle = () => {
		const { toggle } = drawer
		setDrawer({ visible: true, toggle: !toggle })
	}

	return (
		<Row gutter={16}>
			<Col span={12}>
				<h1>Project</h1>
			</Col>
			<Col span={12} style={{ display: 'flex', justifyContent: 'flex-end' }}>
				<Button onClick={handleDrawerToggle}>Add new +</Button>
			</Col>
			<Col span={24}>
				<Table loading={loading} dataSource={projectList.data} columns={columns} rowKey="id"></Table>
			</Col>
			<Drawer drawerToggle={drawer} title="Create a new project">
				<Row gutter={16}>
					<Col span={24}>
						<Form.Item name="name" label="Name" rules={[{ required: true, message: 'Please enter user name' }]}>
							<Input placeholder="Please enter user name" />
						</Form.Item>
					</Col>
					<Col span={24}>
						<Form.Item name="url" label="Url" rules={[{ required: true, message: 'Please enter url' }]}>
							<Input style={{ width: '100%' }} addonBefore="http://" addonAfter=".com" placeholder="Please enter url" />
						</Form.Item>
					</Col>
					<Col span={24}>
						<Form.Item name="owner" label="Owner" rules={[{ required: true, message: 'Please select an owner' }]}>
							<Select placeholder="Please select an owner">
								<Option value="xiao">Xiaoxiao Fu</Option>
								<Option value="mao">Maomao Zhou</Option>
							</Select>
						</Form.Item>
					</Col>
					<Col span={24}>
						<Form.Item name="type" label="Type" rules={[{ required: true, message: 'Please choose the type' }]}>
							<Select placeholder="Please choose the type">
								<Option value="private">Private</Option>
								<Option value="public">Public</Option>
							</Select>
						</Form.Item>
					</Col>
				</Row>
				<Row gutter={16} justify="end">
					<Col span={24}>
						<Button type="primary" htmlType="submit">
							Submit
						</Button>
					</Col>
				</Row>
			</Drawer>
		</Row>
	)
}

export default ProjectList
