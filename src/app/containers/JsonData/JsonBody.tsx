import * as React from 'react'
import { Button, Card, Col, Row } from 'antd'
import Modal from '@components/Modal'
import CodeBlock from '@components/CodeBlock'
import JsonDataForm from '../../components/Form/JsonDataForm'
import { useJsonData } from '@hook/useJsonData'
import { useToggle } from '@hook/useToggle'
interface JsonDataProps {}

const JsonData: React.FC<JsonDataProps> = (props: any) => {
	const { data, loading } = useJsonData()
	const { toggleFlag, setToggleFlag } = useToggle()

	const handleDrawerToggle = () => {
		setToggleFlag({ visible: true, toggle: !toggleFlag })
	}

	return (
		<Row gutter={16}>
			<Col span={12}>
				<h1>Request JSON Body</h1>
			</Col>
			<Col span={12} style={{ display: 'flex', justifyContent: 'flex-end' }}>
				<Button onClick={() => handleDrawerToggle()}>Request Form</Button>
			</Col>
			<Col span={24}>
				<Card title="JSON Body" bordered={false} loading={loading}>
					<CodeBlock>{data}</CodeBlock>
				</Card>
			</Col>
			<Modal itemToggle={toggleFlag} title="Create a new request">
				<JsonDataForm />
			</Modal>
		</Row>
	)
}

export default JsonData
