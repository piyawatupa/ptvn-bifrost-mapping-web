import * as React from 'react'
import { useState } from 'react'
import { Button, Col, Dropdown, Menu, Row, Typography } from 'antd'
import Table from '@components/Table'
import Modal from '@components/Modal'
import { EyeOutlined, DownOutlined, EditOutlined } from '@ant-design/icons'
const { Paragraph } = Typography
import { useToggle } from 'app/hook/useToggle'
import { useMapping } from 'app/hook/useMapping'

interface MappingListProps {}
const CREATE_MAPPING = 'Create a new mapping'
const UPDATE_MAPPING = 'Update mapping'
const MappingList: React.FC<MappingListProps> = (props: any) => {
	const { mappingList, loading } = useMapping()
	const { toggleFlag, setToggleFlag } = useToggle()
	let [titleAction, setTitleAction] = useState(CREATE_MAPPING)

	const handleViewJson = (item: any) => {
	}
	const handleEditProxy = (item: any) => {
		setTitleAction(UPDATE_MAPPING)
	}

	const handleAddProxy = () => {
		setTitleAction(CREATE_MAPPING)
	}


	const columns = [
		{
			title: 'ID',
			dataIndex: 'id',
			render: (text: any, record: any, index: any) => {
				return <Paragraph copyable={{ text: JSON.stringify(record) }}>{record.id}</Paragraph>
			},
		},
		{
			title: 'Name',
			dataIndex: 'name',
		},
		{
			title: 'Description',
			dataIndex: 'description',
		},
		{
			title: 'Action(s)',
			render: (text: any, record: any) => {
				return (
					<Dropdown
						overlay={
							<Menu>
								<Menu.Item key="1" icon={<EditOutlined />} onClick={handleEditProxy}>
									Edit
								</Menu.Item>
								<Menu.Item key="2" icon={<EyeOutlined />} onClick={() => handleViewJson(record)}>
									View JSON
								</Menu.Item>
							</Menu>
						}
					>
						<Button>
							Action(s) <DownOutlined />
						</Button>
					</Dropdown>
				)
			},
		},
	]

	return (
		<Row gutter={16}>
			<Col span={12}>
				<h1>Mapping</h1>
			</Col>
			<Col span={12} style={{ display: 'flex', justifyContent: 'flex-end' }}>
				<Button onClick={handleAddProxy}>Add new +</Button>
			</Col>
			<Col span={24}>
				<Table loading={loading} dataSource={mappingList.data} columns={columns} rowKey="id"></Table>
			</Col>
			{/* <Modal drawerToggle={drawer} title={titleAction}>
				<ProxyForm />
			</Modal> */}
		</Row>
	)
}

export default MappingList
