import * as React from 'react';
// import { connect } from 'react-redux';
// import { bindActionCreators, Dispatch } from 'redux';
// import { authenActions } from 'app/actions/authen.action';
import { RouteComponentProps } from 'react-router-dom';
// import Spinner from '../../components/UI/Spinner';

export namespace Sigin {
  export interface Props extends RouteComponentProps<void> {
    // authenActions: authenActions;
  }
}

class Signout extends React.Component<Sigin.Props, {}> {
  constructor(props: Sigin.Props, context?: any) {
    super(props, context);
  }


  render() {
    return (
      <h1>
        {/* <Spinner loading>{null}</Spinner> */}
        <div
          style={{
            maxHeight: '100%',
            position: 'absolute',
            top: '55%',
            left: '50%',
            transform: 'translate(-55%, -50%)',
          }}
        >
          Sign out
        </div>
      </h1>
    );
  }
}

export default Signout;
