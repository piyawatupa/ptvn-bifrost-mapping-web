import * as React from "react";
import { useEffect } from "react";
// import authenticationService from "../../services/authentication.service";
import Spin from "../../components/Spin";
// import { PageEnum } from "app/utils/enums";
//import { Redirect } from "react-router-dom";

interface SigninProps {}

const Signin: React.FC<SigninProps> = (props: any) => {
  useEffect(() => {
    // handelSignin();
  }, []);

  // const handelSignin = async () => {
  //   let response = await authenticationService.signin();
  //   if (response.status === 200) {
  //     return (window.location.href = PageEnum.HOME);
  //   } else {
  //     return (window.location.href = PageEnum.UNAUTHORIZED);
  //   }
  // };

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        maxHeight: "100%",
        position: "absolute",
        top: "55%",
        left: "50%",
        transform: "translate(-55%, -50%)",
      }}
    >
      <Spin loading></Spin>
      <h1>Verify account</h1>
    </div>
  );
};

export default Signin;
