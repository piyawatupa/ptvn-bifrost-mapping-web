import * as React from 'react'
import { useEffect, useState } from 'react'
import { Alert, Button, Col, Dropdown, Menu, Row, Spin, Typography } from 'antd'
import { DownOutlined } from '@ant-design/icons'
import Drawer from '@components/Drawer'
import Table from '@components/Table'
import ProxyForm from '@components/Form/ProxyForm'
const { Paragraph } = Typography
import { useProxy } from '@hook/useProxy'
import { useMapping } from 'app/hook/useMapping'
import { useProject } from 'app/hook/useProject'
import { useToggle } from 'app/hook/useToggle'
import Modal from '@components/Modal'
import CodeBlock from '@components/CodeBlock'

interface ProxyListProps {}
const CREATE_PROXY = 'Create a new proxy'
const UPDATE_PROXY = 'Update proxy'

const ProxyList: React.FC<ProxyListProps> = (props: any) => {
	const { proxyList, loading, createProxyRes, updateProxyRes } = useProxy()
	const { toggleFlag, handleToggle } = useToggle()
	const [titleAction, setTitleAction] = useState(CREATE_PROXY)
	const [editData, setEditData] = useState(null)
	useEffect(() => {}, [updateProxyRes])

	const renderMethod = (method: any) => {
		switch (method) {
			case 'GET':
				return <span style={{ color: '#259c47' }}>{method}</span>
			case 'POST':
				return <span style={{ color: '#fcb204' }}>{method}</span>
			case 'PUT':
				return <span style={{ color: '#0a7bed' }}>{method}</span>
			default:
				return <span style={{ color: '#b31f1d' }}>{method}</span>
		}
	}

	const handleEditProxy = (item: any) => {
		setEditData({
			...item,
			authentication: JSON.stringify(item.authentication),
			headers: JSON.stringify(item.headers),
		})
		setTitleAction(UPDATE_PROXY)
		handleToggle(true)
	}

	const handleCreateProxy = () => {
		setEditData({
			authentication: '',
			description: '',
			headers: '',
			id: '',
			mappingId: '',
			name: '',
			owner: '',
			projectName: '',
			target: '',
			targetMethod: '',
			touchPoint: '',
		})
		setTitleAction(CREATE_PROXY)
		handleToggle(true)
	}

	const menu = (record: any) => {
		return (
			<Menu>
				<Menu.Item key="1" onClick={() => handleEditProxy(record)}>
					Edit
				</Menu.Item>
			</Menu>
		)
	}

	const columns = [
		{
			title: 'ID',
			dataIndex: 'id',
			render: (text: any, record: any, index: any) => {
				return <Paragraph copyable={{ text: record.id }}>{record.id}</Paragraph>
			},
			width: 300,
		},
		{
			title: 'Name',
			dataIndex: 'name',
			width: 300,
		},
		{
			title: 'URL',
			dataIndex: 'touchPoint',
			render: (text: any, record: any, index: any) => {
				return (
					<Paragraph
						copyable={{
							text: `https://platform-dev.pantavanij.com/bifrost/api/route/${record.projectName}/${record.touchPoint}`,
						}}
					>
						{`https://platform-dev.pantavanij.com/bifrost/api/route/${record.projectName}/${record.touchPoint}`}
					</Paragraph>
				)
			},
			width: 600,
		},
		{
			title: 'Mapping ID',
			dataIndex: 'mappingId',
			width: 300,
		},
		{
			title: 'Method',
			dataIndex: 'targetMethod',
			render: (text: any) => {
				return renderMethod(text)
			},
			width: 200,
		},
		{
			title: 'Action(s)',
			render: (text: any, record: any, index: any) => {
				return (
					<Dropdown overlay={() => menu(record)}>
						<Button>
							Action(s) <DownOutlined />
						</Button>
					</Dropdown>
				)
			},
			width: 150,
			fixed: 'right',
		},
	]

	return (
		<>
			<Row gutter={16}>
				<Col span={12}>
					<h1>Proxy</h1>
				</Col>
				<Col span={12} style={{ display: 'flex', justifyContent: 'flex-end' }}>
					<Button onClick={handleCreateProxy}>Add new +</Button>
				</Col>
			</Row>
			<Row gutter={16}>
				<Col span={24}>
					<Table loading={loading} dataSource={proxyList.data} columns={columns} rowKey="id"></Table>
				</Col>
			</Row>
			{toggleFlag.visible && (
				<Modal itemToggle={toggleFlag} title={titleAction}>
					<ProxyForm data={editData} />
				</Modal>
			)}
		</>
	)
}

export default ProxyList
