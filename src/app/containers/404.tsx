import * as React from "react";
import { Result, Button } from "antd";

interface PageProps {}
const Page: React.FC<PageProps> = (props: any) => {
  return (
    <Result
      status="warning"
      title="There are some problems with your operation."
      extra={
        <Button type="primary" key="console">
          Go Console
        </Button>
      }
    />
  );
};

export default Page;
