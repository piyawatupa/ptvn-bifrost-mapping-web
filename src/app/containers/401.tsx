import * as React from "react";
import { Result, Button } from "antd";
interface PageProps {}
const Page: React.FC<PageProps> = (props: any) => {
  return (
    <Result
      status="warning"
      title="Unauthorized"
      extra={
        <a href='/signin'>
          <Button type="primary" key="console">
            Signin
          </Button>
        </a>
      }
    />
  );
};

export default Page;
