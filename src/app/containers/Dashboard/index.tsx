import * as React from "react";
import { RouteComponentProps } from "react-router";

export namespace Dashboard {
  export interface Props extends RouteComponentProps<void> {}
  export interface State {}
}
class Dashboard extends React.Component<Dashboard.Props, Dashboard.State> {
  constructor(props: Dashboard.Props, context?: any) {
    super(props, context);
  }

  componentDidMount = async () => {};

  render() {
    return <></>;
  }
}

export default Dashboard;
