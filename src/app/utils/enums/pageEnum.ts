export enum PageEnum {
  HOME = '/',
  PROXY_LIST = '/proxy',
  PROXY_CREATE = '/proxy/create',
  PROJECT_LIST = '/project',
  MAPPING_LIST = '/mapping',
  JSONBODY = '/jsonbody',
  REQUEST = '/request',
  RESPONSE = '/response',
  SIGN_IN = '/signin',
  SIGN_OUT = '/signout',
  UNAUTHORIZED = '/unauthorized',
  ERROR = '/error'
}
