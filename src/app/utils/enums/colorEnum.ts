export enum ColorEnum{
    FAIL_COLOR = 'error',
    SUCCESS_COLOR = 'success',
    PROCESS_COLOR = 'processing',
    WARNINNG_COLOR = 'warning'
}