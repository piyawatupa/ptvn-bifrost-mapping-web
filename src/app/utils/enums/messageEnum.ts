
export enum MessageEnum {
    ALREADY_EXISTS_EMAIL_NOT_VERIFY_EXCEED_LIMIT = "Signup.ErrorForm.EmailExistsNotVerifyExceedLimit",
    EMAIL_NOT_FOUND = "Signup.ErrorForm.EmailNotFound",
    ALREADY_EXISTS_EMAIL_NOT_VERIFY = "Signup.ErrorForm.EmailExistsNotVerify",
    ALREADY_EXISTS_EMAIL = "Signup.ErrorForm.EmailExists",

    SEND_MAIL_ERROR = "Error from sending mail service",
    SEND_MAIL_SUCCESS = "[Email Service] Sending email success",
    INTERNAL_SERVER_ERROR = "Internal server error."
}