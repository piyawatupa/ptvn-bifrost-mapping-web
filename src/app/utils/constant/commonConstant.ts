export class CommonConstants {
  public static Dark = "dark";
  public static Light = "body-login light";
  public static Access_Token = "accessToken";
  public static Refresh_Token = "refreshToken";
  public static ConnectToken = "connectToken";
  public static Language = "language";
  public static AuthenToken = "authen_token";
}
