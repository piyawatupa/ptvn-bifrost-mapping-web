import i18n from "./i18n";

export function translate(cell:string) {
  return (i18n.t(cell));
}
