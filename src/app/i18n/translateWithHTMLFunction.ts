import i18n from "./i18n";

export function translateWithHTML(cell:string) {
  return  {__html: (i18n.t(cell))};
}
