import * as React from 'react';
import { Route, Redirect, RouteProps } from 'react-router-dom';
// import { Token } from '../models';
import { AuthenHelper } from '../helper';
import { PageEnum } from 'app/utils/enums';
// import { AuthenHelper } from "../helper/";
//import { PageEnum } from 'app/utils/enums/pageEnum';
export interface PrivateRouteModel extends RouteProps {}

export const PrivateRoute: React.FC<PrivateRouteModel> = (props) => {
  const bypass = true;
  // const authToken = AuthenHelper.getToken();
  // if (validateToken(authToken)) {
  //   return <Route {...props} component={props.component} />;
  // } else {
  //   AuthenHelper.clearToken();
  //   return <Redirect to={PageEnum.UNAUTHORIZED} />;
  // }
  if (bypass) {
    return <Route {...props} component={props.component} />;
  } else {
    AuthenHelper.clearToken();
    return <Redirect to={PageEnum.UNAUTHORIZED} />;
  }
};

// const validateToken = (token: Token) => {
//   if (
//     token === null ||
//     token.access_token === undefined ||
//     token.access_token === ''
//   ) {
//     return false;
//   }

//   return true;
// };

export default PrivateRoute;
