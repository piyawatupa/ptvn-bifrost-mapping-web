import axios from 'axios';
// import { Token } from 'app/models/';
import { AuthenHelper } from '../helper';
import { PageEnum } from 'app/utils/enums';
import authenticationService from 'app/services/authentication.service';
axios.interceptors.request.use(
  async function (config) {
    // Do something before request is sent
    config.url = config.url === '/signin'
    ? 'https://idp-dev.pantavanij.com:8443/auth/realms/ptvn/protocol/openid-connect/token'
    : `${process.env['GATEWAY_URL']}${config.url}`;

    const token: any = AuthenHelper.getToken();


    if (token != null) {
      if (AuthenHelper.isAccessTokenExpire(token.access_token)) {
        if (!AuthenHelper.isRefreshTokenExpire(token.refresh_token)) {
          await renewToken(token);
          const newToken: any = AuthenHelper.getToken();
          config.headers['Authorization'] = `Bearer ${newToken.access_token}`;
          config.headers['Accept'] = '*/*';
          config.headers['Access-Control-Allow-Origin'] = '*';
        }

        if (AuthenHelper.isRefreshTokenExpire(token.refresh_token)) {
          AuthenHelper.clearToken();
          window.location.href = PageEnum.UNAUTHORIZED;
        }
      }

      if (!AuthenHelper.isAccessTokenExpire(token.access_token)) {
        config.headers['Authorization'] = `Bearer ${token.access_token}`;
        config.headers['Accept'] = '*/*';
        config.headers['Access-Control-Allow-Origin'] = '*';
      }
    }

    !process.env.NODE_ENV || process.env.NODE_ENV === 'development'
      ? console.log('Request object', config)
      : null;

    return config;
  },
  function (error) {
    // Do something with request error
    !process.env.NODE_ENV || process.env.NODE_ENV === 'development'
      ? console.log(error)
      : null;
    // window.location.href = PageEnum.NOT_FOUND;
    return Promise.reject(error);
  }
);

axios.interceptors.response.use(
  function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    !process.env.NODE_ENV || process.env.NODE_ENV === 'development'
      ? console.log(response)
      : null;

    return response;
  },
  function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    console.log(error);
    // window.location.href = PageEnum.NOT_FOUND;
    return Promise.reject(error);
  }
);

async function renewToken(token: any) {
  try {
    let result = await authenticationService.signin();
    AuthenHelper.setToken(result);
  } catch (error) {
    console.error('Error:', error);
    AuthenHelper.clearToken();
    window.location.href = PageEnum.UNAUTHORIZED;
  }
}

export const httpClient = axios;
