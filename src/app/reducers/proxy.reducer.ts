import { handleActions, Action } from 'redux-actions'
import { RootState } from './state'
import { proxyActions } from '@actions/proxy.action'
import { proxyListModel, createProxyModel, updateProxyModel } from '@models/proxyModel'

const initialProxyList: RootState.proxyListState = {
	data: [],
	isError: true,
}
export const proxyListReducer = handleActions<RootState.proxyListState, proxyListModel>(
	{
		[proxyActions.Type.FETCH_PROXY_LIST_SUCCESS]: (state, action: Action<proxyListModel>) => {
			return {
				...state,
				data: action.payload!.data,
				isError: false,
			}
		},
		[proxyActions.Type.FETCH_PROXY_LIST_FAIL]: (state, action: Action<proxyListModel>) => {
			return {
				...state,
				loading: false,
				isError: true,
			}
		},
	},
	initialProxyList
)

const initialCreateProxy: RootState.createProxyState = {
	data: {
		id: '',
		authentication: {},
		description: '',
		headers: {},
		mappingId: null,
		name: '',
		owner: '',
		projectName: '',
		target: '',
		targetMethod: '',
		touchPoint: '',
	},
	isError: true,
}
export const createProxyReducer = handleActions<RootState.createProxyState, createProxyModel>(
	{
		[proxyActions.Type.CREATE_PROXY_SUCCESS]: (state, action: Action<createProxyModel>) => {
			return {
				...state,
				data: action.payload!.data,
				isError: false,
			}
		},
		[proxyActions.Type.CREATE_PROXY_FAIL]: (state, action: Action<createProxyModel>) => {
			return {
				...state,
				isError: true,
			}
		},
	},
	initialCreateProxy
)

const initialUpdateProxy: RootState.updateProxyState = {
	data: {
		id: '',
		authentication: {},
		description: '',
		headers: {},
		mappingId: null,
		name: '',
		owner: '',
		projectName: '',
		target: '',
		targetMethod: '',
		touchPoint: '',
	},
	isError: true,
}
export const updateProxyReducer = handleActions<RootState.updateProxyState, updateProxyModel>(
	{
		[proxyActions.Type.UPDATE_PROXY_SUCCESS]: (state, action: Action<updateProxyModel>) => {
			return {
				...state,
				data: action.payload!.data,
				isError: false,
			}
		},
		[proxyActions.Type.UPDATE_PROXY_FAIL]: (state, action: Action<updateProxyModel>) => {
			return {
				...state,
				isError: true,
			}
		},
	},
	initialUpdateProxy
)
