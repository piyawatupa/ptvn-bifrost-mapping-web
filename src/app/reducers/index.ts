import { combineReducers } from 'redux'
import { RootState } from './state'
import { jsonDataReducer } from './jsonData.reducer'
import { proxyListReducer, createProxyReducer, updateProxyReducer } from './proxy.reducer'
import { projectListReducer, createProjectReducer, updateProjectReducer } from './project.reducer'
import { mappingListReducer, createMappingReducer, updateMappingReducer } from './mapping.reducer'
export { RootState }

export const rootReducer = combineReducers<RootState>({
	jsonData: jsonDataReducer as any,
	proxyList: proxyListReducer as any,
	createProxy: createProxyReducer as any,
	updateProxy: updateProxyReducer as any,
	projectList: projectListReducer as any,
	createProject: createProjectReducer as any,
	updateProject: updateProjectReducer as any,
	mappingList: mappingListReducer as any,
	createMapping: createMappingReducer as any,
	updateMapping: updateMappingReducer as any,
})
