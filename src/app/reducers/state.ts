import { jsonDataModel } from '@models/jsonDataModel'
import { proxyListModel, createProxyModel, updateProxyModel } from '@models/proxyModel'
import { projectListModel, createProjectModel, updateProjectModel } from '@models/projectModel'
import { mappingListModel, createMappingModel, updateMappingModel } from '@models/mappingModel'
export interface RootState {
	router?: any
	jsonData: RootState.jsonDataState
	proxyList: RootState.proxyListState
	createProxy: RootState.createProxyState
	updateProxy: RootState.updateProxyState
	projectList: RootState.projectListState
	createProject: RootState.createProjectState
	updateProject: RootState.updateProjectState
	mappingList: RootState.mappingListState
	createMapping: RootState.createMappingState
	updateMapping: RootState.updateMappingState
}

export namespace RootState {
	export type jsonDataState = jsonDataModel
	export type proxyListState = proxyListModel
	export type createProxyState = createProxyModel
	export type updateProxyState = updateProxyModel
	export type projectListState = projectListModel
	export type createProjectState = createProjectModel
	export type updateProjectState = updateProjectModel
	export type mappingListState = mappingListModel
	export type createMappingState = createMappingModel
	export type updateMappingState = updateMappingModel
}
