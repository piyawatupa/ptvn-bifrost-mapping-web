import { handleActions, Action } from 'redux-actions'
import { RootState } from './state'
import { projectActions } from '@actions/project.action'
import { projectListModel, createProjectModel, updateProjectModel } from '@models/projectModel'

const initialProjectList: RootState.projectListState = {
	data: [],
	isError: true,
}
export const projectListReducer = handleActions<RootState.projectListState, projectListModel>(
	{
		[projectActions.Type.FETCH_PROJECT_LIST_SUCCESS]: (state, action: Action<projectListModel>) => {
			return {
				...state,
				data: action.payload!.data,
				isError: false,
			}
		},
		[projectActions.Type.FETCH_PROJECT_LIST_FAIL]: (state, action: Action<projectListModel>) => {
			return {
				...state,
				loading: false,
				isError: true,
			}
		},
	},
	initialProjectList
)

const initialCreateProject: RootState.createProjectState = {
	data: {
		id: '',
		projectName: '',
		description: '',
	},
	isError: true,
}
export const createProjectReducer = handleActions<RootState.createProjectState, createProjectModel>(
	{
		[projectActions.Type.CREATE_PROJECT_SUCCESS]: (state, action: Action<createProjectModel>) => {
			return {
				...state,
				data: action.payload!.data,
				isError: false,
			}
		},
		[projectActions.Type.CREATE_PROJECT_FAIL]: (state, action: Action<createProjectModel>) => {
			return {
				...state,
				isError: true,
			}
		},
	},
	initialCreateProject
)

const initialUpdateProject: RootState.updateProjectState = {
	data: {
		id: '',
		projectName: '',
		description: '',
	},
	isError: true,
}
export const updateProjectReducer = handleActions<RootState.updateProjectState, updateProjectModel>(
	{
		[projectActions.Type.UPDATE_PROJECT_SUCCESS]: (state, action: Action<updateProjectModel>) => {
			return {
				...state,
				data: action.payload!.data,
				isError: false,
			}
		},
		[projectActions.Type.UPDATE_PROJECT_FAIL]: (state, action: Action<updateProjectModel>) => {
			return {
				...state,
				isError: true,
			}
		},
	},
	initialUpdateProject
)
