import { handleActions, Action } from 'redux-actions'
import { RootState } from './state'
import { mappingActions } from '@actions/mapping.action'
import { mappingListModel, createMappingModel, updateMappingModel } from '@models/mappingModel'

const initialMappingList: RootState.mappingListState = {
	data: [],
	isError: true,
}
export const mappingListReducer = handleActions<RootState.mappingListState, mappingListModel>(
	{
		[mappingActions.Type.FETCH_MAPPING_LIST_SUCCESS]: (state, action: Action<mappingListModel>) => {
			return {
				...state,
				data: action.payload!.data,
				isError: false,
			}
		},
		[mappingActions.Type.FETCH_MAPPING_LIST_FAIL]: (state, action: Action<mappingListModel>) => {
			return {
				...state,
				isError: true,
			}
		},
	},
	initialMappingList
)

const initialCreateMapping: RootState.createMappingState = {
	data: {
		id: '',
		request: '',
		response: '',
		description: '',
		name: '',
	},
	isError: true,
}
export const createMappingReducer = handleActions<RootState.createMappingState, createMappingModel>(
	{
		[mappingActions.Type.CREATE_MAPPING_SUCCESS]: (state, action: Action<createMappingModel>) => {
			return {
				...state,
				data: action.payload!.data,
				isError: false,
			}
		},
		[mappingActions.Type.CREATE_MAPPING_FAIL]: (state, action: Action<createMappingModel>) => {
			return {
				...state,
				isError: true,
			}
		},
	},
	initialCreateMapping
)

const initialUpdateMapping: RootState.updateMappingState = {
	data: {
		id: '',
		request: '',
		response: '',
		description: '',
		name: '',
	},
	isError: true,
}
export const updateMappingReducer = handleActions<RootState.updateMappingState, updateMappingModel>(
	{
		[mappingActions.Type.UPDATE_MAPPING_SUCCESS]: (state, action: Action<updateMappingModel>) => {
			return {
				...state,
				data: action.payload!.data,
				isError: false,
			}
		},
		[mappingActions.Type.UPDATE_MAPPING_FAIL]: (state, action: Action<updateMappingModel>) => {
			return {
				...state,
				isError: true,
			}
		},
	},
	initialUpdateMapping
)
