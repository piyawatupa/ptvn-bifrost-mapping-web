import { handleActions, Action } from 'redux-actions'
import { RootState } from './state'
import { jsonDataActions } from '@actions/jsonData.action'
import { jsonDataModel } from '@models/jsonDataModel'

const initialJsonData: RootState.jsonDataState = {
	data: {
		I_DATA_TABLE: {
			item: [
				{
					COMMAND: '',
					TABLE_OR_STRUCT: '',
					FIELD_NAME: '',
					FIELD_VALUE: '',
				},
			],
		},
		E_MESSAGES: {
			item: {},
		},
		E_DATA_TABLE: {
			item: [
				{
					COMMAND: '',
					TABLE_OR_STRUCT: '',
					FIELD_NAME: '',
					FIELD_VALUE: '',
				},
			],
		},
	},
	isError: true,
}
export const jsonDataReducer = handleActions<RootState.jsonDataState, jsonDataModel>(
	{
		[jsonDataActions.Type.FETCH_JSON_DATA_SUCCESS]: (state, action: Action<jsonDataModel>) => {
			return {
				...state,
				data: action.payload!.data,
				isError: false,
			}
		},
		[jsonDataActions.Type.FETCH_JSON_DATA_FAIL]: (state, action: Action<jsonDataModel>) => {
			return {
				...state,
				isError: true,
			}
		},
	},
	initialJsonData
)
