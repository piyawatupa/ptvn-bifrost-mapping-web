import { CommonHelper } from "./";
import { CommonConstants } from "../utils/constant/";
// import { Token } from "app/models/";
import { PageEnum } from "../utils/enums";
const jwt = require("jsonwebtoken");

export class AuthenHelper {
  public static authHeader() {
    // return authorization header with jwt token
    let user = sessionStorage.getItem("access_token");
    return { Authorization: "Bearer " + user };
  }

  public static handleResponse(response: any) {
    if (response.status != 200) {
      if (response.status === 401) {
        this.clearToken();
        const error = (response && response.message) || response.statusText;
        return Promise.reject(error);
      }
    }
    return response;
  }

  public static createAuthenStorage(
    access_token: string,
    refresh_token: string
  ) {
    sessionStorage.setItem("access_token", access_token);
    sessionStorage.setItem("refresh_token", refresh_token);
  }

  public static redirectToUnAuth() {
    location.href = PageEnum.UNAUTHORIZED;
  }

  public static isAccessTokenExpire(accessToken: any) {
    const decodedAccessToken = this.decodeToken(accessToken);
    const accessTokenExpireDateInMilli = decodedAccessToken.exp;
    const currentDateInMilli = CommonHelper.getCurrentDateInMilli();

    return currentDateInMilli > accessTokenExpireDateInMilli;
  }

  public static isRefreshTokenExpire(refreshToken: any) {
    const decodedRefreshToken = this.decodeToken(refreshToken);
    const refreshTokenExpireDateInMilli = decodedRefreshToken.exp;
    const currentDateInMilli = CommonHelper.getCurrentDateInMilli();

    return currentDateInMilli > refreshTokenExpireDateInMilli;
  }

  public static clearToken() {
    sessionStorage.removeItem(CommonConstants.AuthenToken);
    localStorage.removeItem(CommonConstants.Language);
  }

  public static getToken() {
    const sessionToken = this.getSessionStorage();

    if (
      !CommonHelper.isEmptyOrNullOrUndefined(sessionToken) &&
      sessionToken.access_token !== undefined
    ) {
      return sessionToken;
    }

    return null;
  }

  public static getSessionStorage() {
    const sessionToken = sessionStorage.getItem(CommonConstants.AuthenToken);
    const connectJwt = this.jsonParseAuth(sessionToken);
    return connectJwt;
  }

  public static jsonParseAuth(json: string) {
    let result: any = {};
    try {
      return (result = JSON.parse(json));
    } catch {
      return result;
    }
  }

  public static requestTokenConfig() {
    return {
      headers: { "Content-Type": "multipart/form-data" },
      auth: {
        username: "clientId2",
        password: "clientSecret2",
      },
    };
  }

  public static decodeToken(token: string) {
    try {
      return jwt.decode(token);
    } catch {
      return null;
    }
  }

  public static setTokenFirstTime(newToken: any) {
    sessionStorage.setItem(
      CommonConstants.AuthenToken,
      JSON.stringify(newToken)
    );
  }

  public static setToken(newToken: any) {
    const sessionToken = this.getSessionStorage();
    console.log(sessionToken);
    if (
      !CommonHelper.isEmptyOrNullOrUndefined(sessionToken) &&
      sessionToken.access_token !== undefined
    ) {
      return sessionStorage.setItem(
        CommonConstants.AuthenToken,
        JSON.stringify(newToken)
      );
    }
    return null;
  }
}
