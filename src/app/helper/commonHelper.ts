import { notification } from "antd";
const moment = require("moment");

export class CommonHelper {
  public static notificationHandle(
    notificationType: string,
    message: string,
    description: string
  ) {
    switch (notificationType) {
      case "success":
        return notification.success({ message, description });
      case "info":
        return notification.info({ message, description });
      case "warning":
        return notification.warning({ message, description });
      case "error":
        return notification.error({ message, description });
    }
  }

  public static convertTime(date: any) {
    return moment(date, "DD-MM-YYYY hh:mm:ss")
      .add(7, "hours")
      .format("YYYY-MM-DD HH:mm");
  }

  public static getFileName(response: string) {
    const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
    const matches = filenameRegex.exec(response);
    let fileName = matches[1].replace(/['"]/g, "");
    return fileName;
  }

  public static isEmptyOrNullOrSpace(item: string): boolean {
    if (item === undefined) return false;
    else
      return item === null ||
        item.toString() === "" ||
        item.toString().trim() === ""
        ? true
        : false;
  }

  public static isEmptyOrNull(item: string): boolean {
    if (item === undefined) return false;
    else return item === null || item.toString() === "" ? true : false;
  }

  public static isEmptyOrNullOrUndefined(item: string): boolean {
    return item === undefined || item === null || item.toString() === ""
      ? true
      : false;
  }

  public static getCurrentDateInMilli() {
    const currentDateInMilli = Math.floor(moment().valueOf() / 1000);
    return currentDateInMilli;
  }

  public static isJson(str: any) {
    try {
      if (typeof str === "object") {
        return true;
      } else {
        JSON.parse(str);
      }
    } catch (e) {
      const toJsonString = JSON.stringify(str);
      // isJson(toJsonString);
      return false;
    }
    return true;
  }
}
