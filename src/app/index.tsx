import * as React from "react";
import { Route, Switch } from "react-router-dom";
import { RouteComponentProps } from "react-router-dom";
import i18n from "../app/i18n/i18n";
import { PrivateRoute } from "./middleware/privateRoute";
import Home from "./containers/Home";
import Signin from "./containers/Signin";
import Error401 from "./containers/401";
import Error404 from "./containers/404";
import HeaderBar from "app/components/Layouts/Header";
import Signout from "./containers/Signout";
import { PageEnum } from "./utils/enums";

export namespace RouterApp {
  export interface RouterAppProps extends RouteComponentProps<void> {}

  export interface RouterAppState {
    refreshToggle: boolean;
  }
}

class RouterApp extends React.Component<
  RouterApp.RouterAppProps,
  RouterApp.RouterAppState
> {
  constructor(props: RouterApp.RouterAppProps, context?: any) {
    super(props, context);

    this.handleChangeLocale = this.handleChangeLocale.bind(this);
    this.state = {
      refreshToggle: false,
    };
  }

  handleChangeLocale = (lng: string) => {
    i18n.changeLanguage(lng);
  };

  render() {
    return (
      <>
        <Switch>
          <Route exact path={PageEnum.SIGN_IN} component={Signin} />
          <Route exact path={PageEnum.SIGN_OUT} component={Signout} />
          <Route exact path={PageEnum.UNAUTHORIZED} component={Error401} />
          <Route exact path={PageEnum.ERROR} component={Error404} />
          <PrivateRoute path={PageEnum.HOME} component={Home} />
        </Switch>
      </>
    );
  }
}

export default RouterApp;
