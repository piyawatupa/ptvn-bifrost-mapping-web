import { useEffect, useState } from 'react'
import { proxyActions } from '@actions/proxy.action'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from 'app/reducers'

export const useProxy = () => {
	const dispatch = useDispatch()
	const proxyList = useSelector((state: RootState) => state.proxyList)
	const createProxyRes = useSelector((state: RootState) => state.createProxy)
	const updateProxyRes = useSelector((state: RootState) => state.updateProxy)
	let [refreshData, setRefreshData] = useState(false)
	let [loading, setLoading] = useState(false)

	useEffect(() => {
		fetchProxyList()
	}, [])

	useEffect(() => {}, [updateProxyRes,createProxyRes])

	const createProxy = (formBody: any) => {
		dispatch(proxyActions.createProxy(formBody))
	}
	const updateProxy = (formBody: any) => {
		dispatch(proxyActions.updateProxy(formBody))
	}
	const fetchProxyList = async () => {
		setLoading(true)
		await dispatch(proxyActions.fetchProxyList())
		setLoading(false)
	}

	return {
		proxyList,
		createProxyRes,
		updateProxyRes,
		loading,
		refreshData,
		setRefreshData,
		fetchProxyList,
		createProxy,
		updateProxy,
	}
}
