import { useEffect, useState } from 'react'
import { jsonDataActions } from '@actions/jsonData.action'
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from 'app/reducers'

export const useJsonData = () => {
	const { fetchRequestJsonBody } = jsonDataActions
	const dispatch = useDispatch()
	const [loading, setLoading] = useState(false)
	const [refreshData, setRefreshData] = useState(false)
	const { data } = useSelector((state: RootState) => state.jsonData)

	const fetchRequest = (formBody: any) => {
		setLoading(true)
		dispatch(fetchRequestJsonBody(formBody))
		setLoading(false)
	}

	return {
		data,
		loading,
		refreshData,
		setRefreshData,
		setLoading,
		fetchRequest,
	}
}
