import { useEffect, useState } from 'react'
import { mappingActions } from '@actions/mapping.action'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from 'app/reducers'
export const useMapping = () => {
	const dispatch = useDispatch()
	const mappingList = useSelector((state: RootState) => state.mappingList)
	let [refreshData, setRefreshData] = useState(false)
	let [loading, setLoading] = useState(false);
	useEffect(() => {
		fetchMappingList()
	}, [])

	const createMapping = async (formBody: any) => {
		await dispatch(mappingActions.createMapping(formBody))
		fetchMappingList()
	}
	const updateMapping = async (formBody: any) => {
		await dispatch(mappingActions.updateMapping(formBody))
		fetchMappingList()
	}
	const fetchMappingList = async () => {
		setLoading(true)
		await dispatch(mappingActions.fetchMappingList())
		setLoading(false)
	}

	return {
		mappingList,
		refreshData,
		loading,
		setRefreshData,
		fetchMappingList,
		createMapping,
		updateMapping,
	}
}
