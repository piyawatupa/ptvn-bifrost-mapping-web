import { useEffect, useState } from 'react'
import { projectActions } from '@actions/project.action'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from 'app/reducers'

export const useProject = () => {
	const dispatch = useDispatch()
	const projectList = useSelector((state: RootState) => state.projectList)
	let [refreshData, setRefreshData] = useState(false)
	let [loading, setLoading] = useState(false)
	useEffect(() => {
		fetchProjectList()
	}, [])

	const createProject = async (formBody: any) => {
		await dispatch(projectActions.createProject(formBody))
		fetchProjectList()
	}
	const updateProject = async (formBody: any) => {
		await dispatch(projectActions.updateProject(formBody))
		fetchProjectList()
	}
	const fetchProjectList = async () => {
		setLoading(true)
		await dispatch(projectActions.fetchProjectList())
		setLoading(false)
	}

	return {
		projectList,
		refreshData,
		loading,
		setRefreshData,
		fetchProjectList,
		createProject,
		updateProject,
	}
}
