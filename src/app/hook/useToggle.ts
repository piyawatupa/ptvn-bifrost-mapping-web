import { useState } from 'react'
export const useToggle = () => {

	let [toggleFlag, setToggleFlag] = useState({ visible: false, toggle: false })

	const handleToggle = (isVisible: boolean) => {
		setToggleFlag({ visible: isVisible, toggle: !toggleFlag.toggle })
	}
	return { toggleFlag, setToggleFlag, handleToggle }
	
}
