import axios from 'axios'

const mappingService = {
	async fetchMappingList() {
		let data: any
		const loading: boolean = false
		let isError: boolean = true
		await axios
			.get(`${process.env['GATEWAY_URL']}/bifrost/mapping`)
			.then((response) => {
				data = response.data
				isError = false
			})
			.catch((error) => {
				data = []
				console.log(error)
			})

		return { data, loading, isError }
	},

	async createMapping(formValue: any) {
		let data: any
		let isError: boolean = true
		await axios
			.post(`${process.env['GATEWAY_URL']}/bifrost/mapping`, formValue)
			.then((response) => {
				data = response.data
				isError = false
			})
			.catch((error) => {
				data = []
				console.log(error)
			})

		return { data, isError }
	},

	async updateMapping(formValue: any) {
		let data: any
		let isError: boolean = true
		await axios
			.put(`${process.env['GATEWAY_URL']}/bifrost/mapping`, formValue)
			.then((response) => {
				data = response.data
				isError = false
			})
			.catch((error) => {
				data = []
				console.log(error)
			})

		return { data, isError }
	},
}

export default mappingService
