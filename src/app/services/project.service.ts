import axios from 'axios'

const projectService = {
	async fetchProjectList() {
		let data: any
		const loading: boolean = false
		let isError: boolean = true
		await axios
			.get(`${process.env['GATEWAY_URL']}/bifrost/project`)
			.then((response) => {
				data = response.data
				isError = false
			})
			.catch((error) => {
				data = []
				console.log(error)
			})

		return { data, loading, isError }
	},

	async createProject(formValue: any) {
		let data: any
		let isError: boolean = true
		await axios
			.post(`${process.env['GATEWAY_URL']}/bifrost/project`, formValue)
			.then((response) => {
				data = response.data
				isError = false
			})
			.catch((error) => {
				data = []
				console.log(error)
			})

		return { data, isError }
	},

	async updateProject(formValue: any) {
		let data: any
		let isError: boolean = true
		await axios
			.put(`${process.env['GATEWAY_URL']}/bifrost/project`, formValue)
			.then((response) => {
				data = response.data
				isError = false
			})
			.catch((error) => {
				data = []
				console.log(error)
			})

		return { data, isError }
	},
}

export default projectService
