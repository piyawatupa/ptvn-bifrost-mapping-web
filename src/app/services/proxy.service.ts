import { PageEnum } from 'app/utils/enums'
import axios from 'axios'

const proxyService = {
	async fetchProxyList() {
		let data: any
		const loading: boolean = false
		let isError: boolean = true
		await axios
			.get(`${process.env['GATEWAY_URL']}/bifrost/proxies`)
			.then((response) => {
				if (response.status === 200) {
					data = response.data
					isError = false
				} else {
					data = null
				}
			})
			.catch((error) => {
				data = null
				console.log(error)
			})

		return { data, loading, isError }
	},

	async createProxy(formValue: any) {
		let data: any
		let isError: boolean = true
		await axios
			.post(`${process.env['GATEWAY_URL']}/bifrost/proxies`, formValue)
			.then((response) => {
				if (response.status === 200) {
					data = response.data
					isError = false
				} else {
					data = null
				}
			})
			.catch((error) => {
				data = null
				console.log(error)
			})

		return { data, isError }
	},

	async updateProxy(formValue: any) {
		let data: any
		let isError: boolean = true
		await axios
			.put(`${process.env['GATEWAY_URL']}/bifrost/proxies`, formValue)
			.then((response) => {
				console.log(response)
				if (response.status === 200) {
					data = response.data
					isError = false
				} else {
					data = null
				}
			})
			.catch((error) => {
				data = null
				console.log(error)
			})

		return { data, isError }
	},
}

export default proxyService
