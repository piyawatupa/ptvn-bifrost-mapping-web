import axios from 'axios'

const jsonDataService = {
	async fetchRequestJsonBody(formBody: any) {
		let data: any
		let isError: boolean = true
		const { command, action, touchPoint, prNum, companyCode, eId } = formBody
		if (command === '') {
			data = {}
		} else {
			const url = `${process.env['GATEWAY_URL']}/${command}/${action}/${touchPoint}/${prNum}/${companyCode}/${eId}/false`
			await axios
				.get(url)
				.then((response) => {
					data = response.data
					isError = false
				})
				.catch((error) => {
					data = null
					console.log(error)
				})
		}
		return { data, isError }
	},
}

export default jsonDataService
