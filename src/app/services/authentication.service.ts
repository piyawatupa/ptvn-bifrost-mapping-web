import { AuthenHelper } from "../helper";
import axios from "axios";
const authenticationService = {
  async signin() {
    const data =
      "grant_type=client_credentials&client_id=cpf&client_secret=002e48c8-ca27-4b98-951e-9acc1ade157e";
    return axios
      .post("/signin", data)
      .then((response) => {
        AuthenHelper.setTokenFirstTime(response.data);
        return response;
      })
      .catch((error) => {
        return error.response;
      });
  },
};

export default authenticationService;
