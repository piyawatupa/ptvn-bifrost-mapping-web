import { createAction } from 'redux-actions'
import { mappingListModel, createMappingModel, updateMappingModel } from '@models/mappingModel'
import mappingService from '@services/mapping.service'

export namespace mappingActions {
	export enum Type {
		FETCH_MAPPING_LIST_SUCCESS = 'FETCH_MAPPING_LIST_SUCCESS',
		FETCH_MAPPING_LIST_FAIL = 'FETCH_MAPPING_LIST_FAIL',
		CREATE_MAPPING_SUCCESS = 'CREATE_MAPPING_SUCCESS',
		CREATE_MAPPING_FAIL = 'CREATE_MAPPING_FAIL',
		UPDATE_MAPPING_SUCCESS = 'UPDATE_MAPPING_SUCCESS',
		UPDATE_MAPPING_FAIL = 'UPDATE_MAPPING_FAIL',
	}

	export const fetchMappingListSuccessAction = createAction<PartialPick<mappingListModel, 'data' | 'isError'>>(Type.FETCH_MAPPING_LIST_SUCCESS)
	export const fetchMappingListFailAction = createAction<PartialPick<mappingListModel, 'data' | 'isError'>>(Type.FETCH_MAPPING_LIST_FAIL)
	export const createMappingSuccessAction = createAction<PartialPick<createMappingModel, 'data' | 'isError'>>(Type.CREATE_MAPPING_SUCCESS)
	export const createMappingFailAction = createAction<PartialPick<createMappingModel, 'data' | 'isError'>>(Type.CREATE_MAPPING_FAIL)
	export const updateMappingSuccessAction = createAction<PartialPick<createMappingModel, 'data' | 'isError'>>(Type.UPDATE_MAPPING_SUCCESS)
	export const updateMappingFailAction = createAction<PartialPick<createMappingModel, 'data' | 'isError'>>(Type.UPDATE_MAPPING_FAIL)

	export function fetchMappingList() {
		return async (dispatch: any) => {
			const response: mappingListModel = await mappingService.fetchMappingList()
			if (!response.isError) {
				dispatch(fetchMappingListSuccessAction(response))
			} else {
				dispatch(fetchMappingListFailAction(response))
			}
		}
	}

	export function createMapping(formBody: any) {
		return async (dispatch: any) => {
			const response: createMappingModel = await mappingService.createMapping(formBody)
			if (!response.isError) {
				dispatch(createMappingSuccessAction(response))
			} else {
				dispatch(createMappingFailAction(response))
			}
		}
	}

	export function updateMapping(formBody: any) {
		console.log(formBody)
		return async (dispatch: any) => {
			const response: updateMappingModel = await mappingService.updateMapping(formBody)
			console.log(response)
			if (!response.isError) {
				dispatch(updateMappingSuccessAction(response))
			} else {
				dispatch(updateMappingFailAction(response))
			}
		}
	}
}

export type mappingActions = typeof mappingActions
