import { createAction } from 'redux-actions'
import { proxyListModel, createProxyModel, updateProxyModel } from '@models/proxyModel'
import proxyService from '@services/proxy.service'

export namespace proxyActions {
	export enum Type {
		FETCH_PROXY_LIST_SUCCESS = 'FETCH_PROXY_LIST_SUCCESS',
		FETCH_PROXY_LIST_FAIL = 'FETCH_PROXY_LIST_FAIL',
		CREATE_PROXY_SUCCESS = 'CREATE_PROXY_SUCCESS',
		CREATE_PROXY_FAIL = 'CREATE_PROXY_FAIL',
		UPDATE_PROXY_SUCCESS = 'UPDATE_PROXY_SUCCESS',
		UPDATE_PROXY_FAIL = 'UPDATE_PROXY_FAIL',
	}

	export const fetchProxyListSuccessAction = createAction<PartialPick<proxyListModel, 'data' | 'isError'>>(Type.FETCH_PROXY_LIST_SUCCESS)
	export const fetchProxyListFailAction = createAction<PartialPick<proxyListModel, 'data' | 'isError'>>(Type.FETCH_PROXY_LIST_FAIL)
	export const createProxySuccessAction = createAction<PartialPick<createProxyModel, 'data' | 'isError'>>(Type.CREATE_PROXY_SUCCESS)
	export const createProxyFailAction = createAction<PartialPick<createProxyModel, 'data' | 'isError'>>(Type.CREATE_PROXY_FAIL)
	export const updateProxySuccessAction = createAction<PartialPick<createProxyModel, 'data' | 'isError'>>(Type.UPDATE_PROXY_SUCCESS)
	export const updateProxyFailAction = createAction<PartialPick<createProxyModel, 'data' | 'isError'>>(Type.UPDATE_PROXY_FAIL)

	export function fetchProxyList() {
		return async (dispatch: any) => {
			const response: proxyListModel = await proxyService.fetchProxyList()
			if (!response.isError) {
				dispatch(fetchProxyListSuccessAction(response))
			} else {
				dispatch(fetchProxyListFailAction(response))
			}
		}
	}

	export function createProxy(formBody: any) {
		return async (dispatch: any) => {
			const response: createProxyModel = await proxyService.createProxy(formBody)
			if (!response.isError) {
				dispatch(createProxySuccessAction(response))
			} else {
				dispatch(createProxyFailAction(response))
			}
		}
	}

	export function updateProxy(formBody: any) {
		return async (dispatch: any) => {
			const response: updateProxyModel = await proxyService.updateProxy(formBody)
			if (!response.isError) {
				dispatch(updateProxySuccessAction(response))
			} else {
				dispatch(updateProxyFailAction(response))
			}
		}
	}
}

export type proxyActions = typeof proxyActions
