import { createAction } from 'redux-actions'
import { jsonDataModel } from '@models/jsonDataModel'

import jsonDataService from '../services/jsonData.service'

export namespace jsonDataActions {
	export enum Type {
		FETCH_JSON_DATA_SUCCESS = 'FETCH_JSON_DATA_SUCCESS',
		FETCH_JSON_DATA_FAIL = 'FETCH_JSON_DATA_FAIL',
	}

	export const fetchJsonDataSuccessAction = createAction<PartialPick<jsonDataModel, 'data' | 'isError'>>(Type.FETCH_JSON_DATA_SUCCESS)
	export const fetchJsonDataFailAction = createAction<PartialPick<jsonDataModel, 'data' | 'isError'>>(Type.FETCH_JSON_DATA_FAIL)

	export function fetchRequestJsonBody(formBody: any) {
		return async (dispatch: any) => {
			const response: jsonDataModel = await jsonDataService.fetchRequestJsonBody(formBody)
			if (!response.isError) {
				dispatch(fetchJsonDataSuccessAction(response))
			} else {
				dispatch(fetchJsonDataFailAction(response))
			}
		}
	}
}

export type jsonDataActions = typeof jsonDataActions
