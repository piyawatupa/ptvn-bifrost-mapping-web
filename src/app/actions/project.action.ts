import { createAction } from 'redux-actions'
import { projectListModel, createProjectModel, updateProjectModel } from '@models/projectModel'
import projectService from '@services/project.service'

export namespace projectActions {
	export enum Type {
		FETCH_PROJECT_LIST_SUCCESS = 'FETCH_PROJECT_LIST_SUCCESS',
		FETCH_PROJECT_LIST_FAIL = 'FETCH_PROJECT_LIST_FAIL',
		CREATE_PROJECT_SUCCESS = 'CREATE_PROJECT_SUCCESS',
		CREATE_PROJECT_FAIL = 'CREATE_PROJECT_FAIL',
		UPDATE_PROJECT_SUCCESS = 'UPDATE_PROJECT_SUCCESS',
		UPDATE_PROJECT_FAIL = 'UPDATE_PROJECT_FAIL',
	}

	export const fetchProjectListSuccessAction = createAction<PartialPick<projectListModel, 'data' | 'isError'>>(Type.FETCH_PROJECT_LIST_SUCCESS)
	export const fetchProjectListFailAction = createAction<PartialPick<projectListModel, 'data' | 'isError'>>(Type.FETCH_PROJECT_LIST_FAIL)
	export const createProjectSuccessAction = createAction<PartialPick<createProjectModel, 'data' | 'isError'>>(Type.CREATE_PROJECT_SUCCESS)
	export const createProjectFailAction = createAction<PartialPick<createProjectModel, 'data' | 'isError'>>(Type.CREATE_PROJECT_FAIL)
	export const updateProjectSuccessAction = createAction<PartialPick<createProjectModel, 'data' | 'isError'>>(Type.UPDATE_PROJECT_SUCCESS)
	export const updateProjectFailAction = createAction<PartialPick<createProjectModel, 'data' | 'isError'>>(Type.UPDATE_PROJECT_FAIL)

	export function fetchProjectList() {
		return async (dispatch: any) => {
			const response: projectListModel = await projectService.fetchProjectList()
			if (!response.isError) {
				dispatch(fetchProjectListSuccessAction(response))
			} else {
				dispatch(fetchProjectListFailAction(response))
			}
		}
	}

	export function createProject(formBody: any) {
		return async (dispatch: any) => {
			const response: createProjectModel = await projectService.createProject(formBody)
			if (!response.isError) {
				dispatch(createProjectSuccessAction(response))
			} else {
				dispatch(createProjectFailAction(response))
			}
		}
	}

	export function updateProject(formBody: any) {
		console.log(formBody)
		return async (dispatch: any) => {
			const response: updateProjectModel = await projectService.updateProject(formBody)
			console.log(response)
			if (!response.isError) {
				dispatch(updateProjectSuccessAction(response))
			} else {
				dispatch(updateProjectFailAction(response))
			}
		}
	}
}

export type projectActions = typeof projectActions
