/** TodoMVC model definitions **/

export interface projectListModel {
	data: Array<{
		id: string
		projectName: string
		description: string
	}>
	isError: boolean
}
export interface createProjectModel {
	data: {
		id: string
		projectName: string
		description: string
	}
	isError: boolean
}

export interface updateProjectModel {
	data: {
		id: string
		projectName: string
		description: string
	}
	isError: boolean
}
