import { RouteProps } from "react-router-dom";

export interface PrivateRouteModel extends RouteProps {
  isAuthenticated?: boolean;
}
