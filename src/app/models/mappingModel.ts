/** TodoMVC model definitions **/

export interface mappingListModel {
	data: Array<{
		id: string
		request: string
		response: string
		description: string
		name: string
	}>
	isError: boolean
}
export interface createMappingModel {
	data: {
		id: string
		request: string
		response: string
		description: string
		name: string
	}
	isError: boolean
}

export interface updateMappingModel {
	data: {
		id: string
		request: string
		response: string
		description: string
		name: string
	}
	isError: boolean
}
