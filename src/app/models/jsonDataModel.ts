/** TodoMVC model definitions **/

export interface jsonDataModel {
  data: {
    I_DATA_TABLE: {
      item: Array<{
        COMMAND: string;
        TABLE_OR_STRUCT: string;
        FIELD_NAME: string;
        FIELD_VALUE: string;
      }>;
    };
    E_MESSAGES: {
      item: {};
    };
    E_DATA_TABLE: {
      item: Array<{
        COMMAND: string;
        TABLE_OR_STRUCT: string;
        FIELD_NAME: string;
        FIELD_VALUE: string;
      }>;
    };
  };
  isError: boolean;
}
