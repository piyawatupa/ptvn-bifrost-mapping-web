/** TodoMVC model definitions **/

export interface proxyListModel {
	data: Array<{
		authentication: {}
		description: string
		headers: {}
		id: string
		mappingId: any
		name: string
		owner: string
		projectName: string
		target: string
		targetMethod: string
		touchPoint: string
	}>
	isError: boolean
}
export interface createProxyModel {
	data: {
		id: string
		authentication: {}
		description: string
		headers: {}
		mappingId: any
		name: string
		owner: string
		projectName: string
		target: string
		targetMethod: string
		touchPoint: string
	}
	isError: boolean
}

export interface updateProxyModel {
	data: {
		id: string
		authentication: {}
		description: string
		headers: {}
		mappingId: any
		name: string
		owner: string
		projectName: string
		target: string
		targetMethod: string
		touchPoint: string
	}
	isError: boolean
}
