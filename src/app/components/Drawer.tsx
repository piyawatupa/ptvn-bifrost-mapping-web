import * as React from 'react'
import { Button, Drawer } from 'antd'
import { useEffect, useState } from 'react'
interface DrawerProps {
	drawerToggle: { visible: boolean; toggle: boolean }
	title: string
}
const DrawerComponent: React.FC<DrawerProps> = ({ drawerToggle, title, children }) => {
  let [visible, setVisible] = useState(false)
  
	useEffect(() => {
		setVisible(drawerToggle.visible)
	}, [drawerToggle.toggle])

	const onClose = () => {
		setVisible(false)
	}

	return (
		<Drawer
			title={title}
			width={800}
			onClose={onClose}
			visible={visible}
			bodyStyle={{ paddingBottom: 80 }}
			footer={
				<div
					style={{
						textAlign: 'right',
					}}
				>
					<Button style={{ marginRight: 8 }} onClick={onClose}>
						Cancel
					</Button>
					<Button htmlType="submit" form="form" key="submit" type="primary" onClick={onClose}>
						Submit
					</Button>
				</div>
			}
		>
			{children}
		</Drawer>
	)
}

export default DrawerComponent
