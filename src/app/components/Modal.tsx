import * as React from 'react'
import { Alert, Button, Modal, Spin } from 'antd'
import { useEffect, useState } from 'react'
interface ModalProps {
	itemToggle: { visible: boolean; toggle: boolean }
	title: string
}
const ModalComponent: React.FC<ModalProps> = ({ itemToggle, title, children }) => {
	const [visible, setVisible] = useState(false)
	useEffect(() => {
		setVisible(itemToggle.visible)
	}, [itemToggle.toggle])

	const onCancel = () => {
		setVisible(false)
	}
	const onSubmit = () => {}

	return (
		<Modal
			closable={true}
			style={{ top: 10, bottom: 10, height: '100vh', maxHeight: '100vh' }}
			title={title}
			width={'100%'}
			visible={visible}
			onCancel={onCancel}
			destroyOnClose
			footer={
				<div
					style={{
						textAlign: 'right',
					}}
				>
					<Button style={{ marginRight: 8 }} onClick={onCancel}>
						Cancel
					</Button>
					<Button htmlType="submit" form="form" key="submit" type="primary">
						Submit
					</Button>
				</div>
			}
		>
			{children}
		</Modal>
	)
}

export default ModalComponent
