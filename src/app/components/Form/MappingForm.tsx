import * as React from 'react'
import { useEffect, useState } from 'react'
import { Col, Form, Input, Row, Select } from 'antd'
import TextArea from 'antd/lib/input/TextArea'
import { useMapping } from 'app/hook/useMapping'
import { useProject } from 'app/hook/useProject'
import { useProxy } from 'app/hook/useProxy'
import CodeBlock from 'app/components/CodeBlock'
import { CommonHelper } from 'app/helper'
import { useToggle } from 'app/hook/useToggle'
const { Option } = Select

interface MappingFormProps {
	data?: any
}

const MappingForm: React.FC<MappingFormProps> = ({ data }) => {
	const [form] = Form.useForm()
	const { mappingList } = useMapping()
	const { updateProxy, createProxy } = useProxy()
	const { handleToggle } = useToggle()
	let [requestBody, setRequestBody] = useState('')
	let [responseBody, setResponseBody] = useState('')

	const handleFormSubmit = (formValue: any) => {
		if (data.id) {
			updateProxy({ ...formValue, authentication: JSON.parse(formValue.authentication), headers: JSON.parse(formValue.headers) })
		} else {
			createProxy({ ...formValue, authentication: JSON.parse(formValue.authentication), headers: JSON.parse(formValue.headers) })
		}
		handleToggle(false)
	}

	const handleFormValueChange = (changedValues: any, allValues: any) => {
		let { authentication, headers } = allValues
		authentication = CommonHelper.isJson(authentication) ? JSON.parse(authentication) : authentication
		headers = CommonHelper.isJson(headers) ? JSON.parse(headers) : headers
		setRequestBody(authentication)
		setResponseBody(headers)
	}

	useEffect(() => {
		form.setFieldsValue(data)
	}, [data])

	return (
		<Form form={form} layout="vertical" id="form" onFinish={handleFormSubmit} initialValues={data} onValuesChange={handleFormValueChange}>
			<Row gutter={16}>
				{data.id && (
					<Col span={24}>
						<Form.Item key="id" name="id" label="Mapping ID">
							<Input readOnly />
						</Form.Item>
					</Col>
				)}
				<Col span={24}>
					<Form.Item key="name" name="name" label="Name" rules={[{ required: true, message: 'Please enter mapping name' }]}>
						<Input placeholder="Please enter mapping name" />
					</Form.Item>
				</Col>
				<Col span={24}>
					<Form.Item key="description" name="description" label="Description" rules={[{ required: true, message: 'Please enter mapping description' }]}>
						<TextArea placeholder="Please enter mapping description"></TextArea>
					</Form.Item>
				</Col>
			</Row>
			<Row gutter={16}>
				<Col span={12}>
					<Form.Item
						key="request"
						name="request"
						label="Request"
						rules={[
							{
								required: false,
								message: 'Please enter request',
							},
						]}
					>
						<TextArea placeholder="Please enter request body"></TextArea>
					</Form.Item>
				</Col>
				<Col span={12}>
					<p style={{ marginBottom: '8px' }}>Request Preview</p>
					{data.request !== '' && requestBody === '' ? <CodeBlock>{JSON.parse(data.authentication)}</CodeBlock> : <CodeBlock>{requestBody}</CodeBlock>}
				</Col>
			</Row>
			<Row gutter={16}>
				<Col span={12}>
					<Form.Item key="response" name="response" label="Response Body" rules={[{ required: false, message: 'Please enter headers body' }]}>
						<TextArea placeholder="Please enter response body"></TextArea>
					</Form.Item>
				</Col>
				<Col span={12}>
					<p style={{ marginBottom: '8px' }}>Response Preview</p>
					{data.response !== '' && responseBody === '' ? <CodeBlock>{JSON.parse(data.response)}</CodeBlock> : <CodeBlock>{responseBody}</CodeBlock>}
				</Col>
				<Col span={24}>
					<Form.Item key="proxyId" name="proxyId" label="Mapping Proxy" rules={[{ required: false }]}>
						{mappingList === null ? (
							<Input placeholder="Please enter proxy id" />
						) : (
							<Select placeholder="Please select proxy">
								<Option value={null}>Select mapping</Option>
								{mappingList.data.map((item: any) => {
									return (
										<Option value={item.id} key={item.id}>
											{item.name}
										</Option>
									)
								})}
							</Select>
						)}
					</Form.Item>
				</Col>
			</Row>
		</Form>
	)
}

export default MappingForm
