import * as React from "react";
import { useState } from "react";
import { Button, Col, Form, Input, Row, Select } from "antd";
import { useJsonData } from "app/hook/useJsonData";
const { Option } = Select;
const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 },
};
interface JsonDataProps {}
const JsonData: React.FC<JsonDataProps> = (props: any) => {
  const { fetchRequest } = useJsonData();
  let [command, setCommand] = useState(null);

  const handlerSelectCommandOnchange = (response: any) => {
    setCommand(response);
  };

  const handlerFormSubmit = (response: any) => {
    fetchRequest(response);
  };
  return (
    <Form
      // {...layout}
      name="basic"
      initialValues={{ remember: true }}
      onFinish={handlerFormSubmit}
      layout="vertical"
      id="form"
    >
      <Row gutter={16}>
        <Col span={12}>
          <Form.Item
            name="command"
            label="Command"
            rules={[{ required: false, message: "Please select command" }]}
          >
            <Select
              placeholder="Please select command"
              onChange={handlerSelectCommandOnchange}
            >
              <Option value={null}>Select Command</Option>
              <Option value="pr">PR</Option>
              <Option value="po">PO</Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
      {command !== null && (
        <Row gutter={16}>
          <Col span={12}>
            <Form.Item
              name="action"
              label="Action"
              rules={[{ required: true, message: "Please select action" }]}
              initialValue={"request"}
            >
              <Select placeholder="Please select action">
                <Option value="">Select type</Option>
                <Option value="request">Request</Option>
                <Option value="response">Response</Option>
              </Select>
            </Form.Item>
          </Col>
        </Row>
      )}

      {command === "pr" && (
        <Row gutter={16}>
          <Col span={12}>
            <Form.Item
              name="touchPoint"
              label="Touch Point"
              rules={[{ required: true, message: "Please select touch point" }]}
              initialValue={"create"}
            >
              <Select placeholder="Please select touch point">
                <Option value={null}>Select touch point</Option>
                <Option value="create">Create</Option>
                <Option value="change">Change</Option>
                <Option value="reject">Reject</Option>
                <Option value="cancel">Cancel</Option>
                <Option value="lastApprove">Last Approve</Option>
              </Select>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="prNum"
              label="PR Number"
              rules={[{ required: true, message: "Please enter pr number" }]}
              initialValue={"9970216"}
            >
              <Input placeholder="Please enter pr number" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="companyCode"
              label="Company Code"
              rules={[{ required: true, message: "Please enter company code" }]}
              initialValue={"333"}
            >
              <Input placeholder="Please enter company code" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="eId"
              label="EID"
              rules={[{ required: true, message: "Please enter EID" }]}
              initialValue={"24"}
            >
              <Input placeholder="Please enter EID" />
            </Form.Item>
          </Col>
        </Row>
      )}
    </Form>
  );
};

export default JsonData;
