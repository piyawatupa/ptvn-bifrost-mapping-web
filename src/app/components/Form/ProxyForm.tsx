import * as React from 'react'
import { useEffect, useState } from 'react'
import { Alert, Col, Form, Input, Row, Select } from 'antd'
import TextArea from 'antd/lib/input/TextArea'
import { useMapping } from '@hook/useMapping'
import { useProject } from '@hook/useProject'
import { useProxy } from '@hook/useProxy'
import CodeBlock from '@components/CodeBlock'
import { CommonHelper } from 'app/helper'
const { Option } = Select

interface ProxyFormProps {
	data?: any
}

const ProxyForm: React.FC<ProxyFormProps> = ({ data }) => {
	const [form] = Form.useForm()
	const { mappingList } = useMapping()
	const { projectList } = useProject()
	const { updateProxy, createProxy, updateProxyRes, createProxyRes } = useProxy()
	const [authneticationBody, setAuthneticationBody] = useState('')
	const [headersBody, setHeaderBody] = useState('')
	const [responseData, setResponseData] = useState({})
	const [alertVisible, setAlertVisible] = useState({ isVisible: false, type: 'success' })
	const [loading, setLoading] = useState(false)
	useEffect(() => {
		form.setFieldsValue(data)
	}, [data])

	useEffect(() => {
		if (data.id) {
			if (updateProxyRes.data.id !== data.id) {
				setResponseData({})
			} else {
				if (!updateProxyRes.isError) {
					setResponseData(updateProxyRes.data)
					if (updateProxyRes.isError) {
						setAlertVisible({ isVisible: true, type: 'error' })
					} else {
						setAlertVisible({ isVisible: true, type: 'success' })
					}
				}
			}
		} else {
			if (createProxyRes.data.id !== data.id) {
				setResponseData({})
			} else {
				setResponseData(createProxyRes.data)
				if (createProxyRes.isError) {
					setAlertVisible({ isVisible: true, type: 'error' })
				} else {
					setAlertVisible({ isVisible: true, type: 'success' })
				}
			}
		}
		setLoading(false)
	}, [data, updateProxyRes, createProxyRes])

	const handleFormSubmit = async (formValue: any) => {
		setLoading(true)
		const authentication = JSON.parse(formValue.authentication)
		const headers = JSON.parse(formValue.headers)
		if (data.id) {
			updateProxy({ ...formValue, authentication, headers })
		} else {
			createProxy({ ...formValue, authentication, headers })
		}
	}

	const handleFormValueChange = (changedValues: any, allValues: any) => {
		let { authentication, headers } = allValues
		authentication = CommonHelper.isJson(authentication) ? JSON.parse(authentication) : authentication
		headers = CommonHelper.isJson(headers) ? JSON.parse(headers) : headers
		setAuthneticationBody(authentication)
		setHeaderBody(headers)
	}

	return (
		<Row gutter={16}>
			{alertVisible.isVisible && alertVisible.type === 'success' && (
				<Col span={24} style={{ marginBottom: '20px' }}>
					<Alert message="Success" type="success" closable></Alert>
				</Col>
			)}
			{/* {alertVisible.isVisible && alertVisible.type === 'error' && (
				<Col span={24} style={{ marginBottom: '20px' }}>
					<Alert message="Error" type="error" closable></Alert>
				</Col>
			)} */}
			<Col span={12}>
				<Form form={form} layout="vertical" id="form" onFinish={handleFormSubmit} initialValues={data} onValuesChange={handleFormValueChange}>
					<Row gutter={16}>
						{data.id && (
							<Col span={24}>
								<Form.Item key="id" name="id" label="Proxy ID">
									<Input readOnly />
								</Form.Item>
							</Col>
						)}
						<Col span={24}>
							<Form.Item key="name" name="name" label="Name" rules={[{ required: true, message: 'Please enter proxy name' }]}>
								<Input placeholder="Please enter proxy name" />
							</Form.Item>
						</Col>
						<Col span={24}>
							<Form.Item key="description" name="description" label="Description" rules={[{ required: true, message: 'Please enter proxy description' }]}>
								<TextArea placeholder="Please enter proxy description"></TextArea>
							</Form.Item>
						</Col>
						<Col span={24}>
							<Form.Item key="owner" name="owner" label="Owner Name" rules={[{ required: true, message: 'Please enter proxy owner name' }]}>
								<Input placeholder="Please enter proxy owner name" />
							</Form.Item>
						</Col>
						<Col span={24}>
							<Form.Item key="target" name="target" label="Target" rules={[{ required: true, message: 'Please enter target url' }]}>
								<Input placeholder="Please enter target url" />
							</Form.Item>
						</Col>
						<Col span={24}>
							<Form.Item key="targetMethod" name="targetMethod" label="Target Method" rules={[{ required: true, message: 'Please select target method' }]}>
								<Select placeholder="Please select target method">
									<Option value="">Select target method</Option>
									<Option value="GET">GET</Option>
									<Option value="POST">POST</Option>
									<Option value="PUT">PUT</Option>
									<Option value="DELETE">DELETE</Option>
								</Select>
							</Form.Item>
						</Col>
					</Row>
					<Row gutter={16}>
						<Col span={12}>
							<Form.Item
								key="authentication"
								name="authentication"
								label="Authentication Body"
								rules={[
									{
										required: false,
										message: 'Please enter authentication body',
									},
								]}
							>
								<TextArea placeholder="Please enter authentication body"></TextArea>
							</Form.Item>
						</Col>
						<Col span={12}>
							<p style={{ marginBottom: '8px' }}>Authentication JSON Body Preview</p>
							{data.authentication !== '' && authneticationBody === '' ? <CodeBlock>{JSON.parse(data.authentication)}</CodeBlock> : <CodeBlock>{authneticationBody}</CodeBlock>}
						</Col>
					</Row>
					<Row gutter={16}>
						<Col span={12}>
							<Form.Item key="headers" name="headers" label="Headers Body" rules={[{ required: false, message: 'Please enter headers body' }]}>
								<TextArea placeholder="Please enter headers body"></TextArea>
							</Form.Item>
						</Col>
						<Col span={12}>
							<p style={{ marginBottom: '8px' }}>Headers JSON Body Preview</p>
							{data.headers !== '' && headersBody === '' ? <CodeBlock>{JSON.parse(data.headers)}</CodeBlock> : <CodeBlock>{headersBody}</CodeBlock>}
						</Col>
						<Col span={24}>
							<Form.Item key="projectName" name="projectName" label="Project" rules={[{ required: true, message: 'Please enter/select project' }]}>
								{projectList === null ? (
									<Input placeholder="Please enter project name" />
								) : (
									<Select placeholder="Please select project">
										<Option value="">Select project name</Option>
										{projectList.data.map((item: any) => {
											return (
												<Option value={item.projectName} key={item.id}>
													{item.projectName}
												</Option>
											)
										})}
									</Select>
								)}
							</Form.Item>
						</Col>
						<Col span={24}>
							<Form.Item key="touchPoint" name="touchPoint" label="Touch Point" rules={[{ required: true, message: 'Please enter touch point name' }]}>
								<Input placeholder="Please enter touch point name" />
							</Form.Item>
						</Col>
						<Col span={24}>
							<Form.Item key="mappingId" name="mappingId" label="Mapping" rules={[{ required: false }]}>
								{mappingList === null ? (
									<Input placeholder="Please enter mapping id" />
								) : (
									<Select placeholder="Please select mapping">
										<Option value={null}>Select mapping</Option>
										{mappingList.data.map((item: any) => {
											return (
												<Option value={item.id} key={item.id}>
													{item.name}
												</Option>
											)
										})}
									</Select>
								)}
							</Form.Item>
						</Col>
					</Row>
				</Form>
			</Col>
			<Col span={12}>
				<CodeBlock>{responseData}</CodeBlock>
			</Col>
		</Row>
	)
}

export default ProxyForm
