import { LoadingOutlined } from "@ant-design/icons";
import { Table as AntdTable } from "antd";
import * as React from "react";

interface TableProps {
  dataSource: any;
  rowKey?: string;
  columns: Array<any>;
  loading: boolean;
}
const Table: React.FC<TableProps> = ({
  dataSource,
  rowKey,
  columns,
  loading,
}) => {
  const spin = <LoadingOutlined style={{ fontSize: 24 }} spin />;
  dataSource = dataSource.length === 0 ? [] : dataSource;

  return (
    <AntdTable
      loading={{
        spinning: loading,
        indicator: spin,
      }}
      
      dataSource={dataSource}
      columns={columns}
      rowKey={rowKey}
      pagination={{
        defaultPageSize: 10,
      }}
      scroll={{ x: 530, y:720 }}
    />
  );
};

export default Table;
