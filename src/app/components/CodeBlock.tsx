import SyntaxHighlighter from "react-syntax-highlighter";
import { docco } from "react-syntax-highlighter/dist/cjs/styles/hljs";
import * as React from "react";
import { CommonHelper } from "app/helper";
interface CodeBlockProps {}
const CodeBlock: React.FC<CodeBlockProps> = ({ children }) => {

  return (
    <SyntaxHighlighter
      language="json"
      style={docco}
      wrapLines={true}
    >
      {CommonHelper.isJson(children) ? JSON.stringify(children, null, "  ") : " "}
    </SyntaxHighlighter>
  );
};

export default CodeBlock;
