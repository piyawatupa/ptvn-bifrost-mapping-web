import { Spin as AntdSpin } from "antd";
import { LoadingOutlined } from "@ant-design/icons";
import * as React from "react";

interface SpinProps {
  loading:boolean
}
const Spin: React.FC<SpinProps> = ({loading}) => {
  const antIcon = <LoadingOutlined style={{ fontSize: 48 }} spin />;
  return <AntdSpin spinning={loading} indicator={antIcon} />;
};

export default Spin;
