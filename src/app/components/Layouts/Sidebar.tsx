import * as React from 'react'
import { SwapOutlined, HomeOutlined, FileTextOutlined, UnorderedListOutlined, DatabaseOutlined } from '@ant-design/icons'
import { Layout, Menu } from 'antd'
import { Link, withRouter } from 'react-router-dom'
import styled from 'styled-components'
import { PageEnum } from 'app/utils/enums'
const { Sider } = Layout
const CustomSider = styled(Sider)`
	background: #fff;
	color: #000;
`
interface MainSiderProps {}
const MainSider: React.FC<MainSiderProps> = (props: any) => {
	const { location } = props
	const parentPathArr = location.pathname.split('/')
	const parentPath = '/' + parentPathArr[1]

	return (
		<CustomSider trigger collapsed={props.children.data.collapsed}>
			<Menu theme="light" mode="inline" defaultSelectedKeys={[PageEnum.HOME]} defaultOpenKeys={['report', 'supplier']} selectedKeys={[parentPath]}>
				<Menu.Item key={PageEnum.HOME}>
					<Link to={PageEnum.HOME}>
						<HomeOutlined />
						<span className="nav-text">Home</span>
					</Link>
				</Menu.Item>
				<Menu.Item key={PageEnum.PROJECT_LIST}>
					<Link to={PageEnum.PROJECT_LIST}>
						<DatabaseOutlined />
						<span className="nav-text">Project</span>
					</Link>
				</Menu.Item>

				<Menu.Item key={PageEnum.PROXY_LIST}>
					<Link to={PageEnum.PROXY_LIST}>
						<SwapOutlined />
						<span className="nav-text">Proxy</span>
					</Link>
				</Menu.Item>

				<Menu.Item key={PageEnum.MAPPING_LIST}>
					<Link to={PageEnum.MAPPING_LIST}>
						<UnorderedListOutlined />
						<span className="nav-text">Mapping</span>
					</Link>
				</Menu.Item>

				<Menu.Item key={PageEnum.JSONBODY}>
					<Link to={PageEnum.JSONBODY}>
						<FileTextOutlined />
						<span className="nav-text">Json Body</span>
					</Link>
				</Menu.Item>

				{/* <Menu.Item key={PageEnum.SIGN_OUT}>
          <Link to={PageEnum.SIGN_OUT}>
            <LogoutOutlined />
            <span className="nav-text">Sign out</span>
          </Link>
        </Menu.Item> */}
			</Menu>
		</CustomSider>
	)
}

export default withRouter(MainSider)
