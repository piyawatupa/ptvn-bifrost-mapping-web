import * as React from 'react'
import { useState } from 'react'
import { Layout } from 'antd'
import styled from 'styled-components'
const { Content } = Layout
import Sidebar from './Sidebar'
import HeaderBar from './Header'
const ContentCustom = styled(Content)`
	padding: 24px;
`

interface MainLayoutProps {
	user?: object
}

const MainLayout: React.FC<MainLayoutProps> = (props: any) => {
	const [collapsed, setCollapsed] = useState(false)
	const onCollapse = () => {
		setCollapsed(!collapsed)
	}
	return (
		<>
			<HeaderBar collapsed={collapsed} onCollapse={onCollapse}></HeaderBar>
			<Layout>
				<Sidebar>{{ data: { collapsed } }}</Sidebar>
				<ContentCustom>{props.children}</ContentCustom>
			</Layout>
		</>
	)
}

export default MainLayout
