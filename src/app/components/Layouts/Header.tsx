import * as React from 'react'
import { Layout } from 'antd'
// import { CommonConstants } from '../../../app/utils/constant';
import { Link } from 'react-router-dom'
import { MenuUnfoldOutlined, MenuFoldOutlined } from '@ant-design/icons'
const { Header } = Layout
interface MainHeaderProps {
	collapsed: boolean
	onCollapse: Function
}

const MainHeader: React.FC<MainHeaderProps> = ({ collapsed, onCollapse }) => {
	return (
		<Header>
			<>
				{React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
					className: 'trigger',
					onClick: () => onCollapse(),
				})}

				<Link to="/" className="logo">
					<span style={{}}>Pantavanij Co., Ltd.</span>
					<span
						style={{
							color: '#8d8d94',
							marginLeft: '8px',
							fontSize: '14px',
						}}
					>
						- Headquarter
					</span>
				</Link>
			</>
		</Header>
	)
}

export default MainHeader
