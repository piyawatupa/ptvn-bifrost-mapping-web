import { Store, createStore, applyMiddleware, compose } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import { logger } from '../middleware/logger'
import { RootState, rootReducer } from '../reducers'
import thunkMiddleware from 'redux-thunk'

export function configureStore(initialState?: RootState): Store<RootState> {
	let middleware = compose(applyMiddleware(logger, thunkMiddleware))

	if (process.env.NODE_ENV !== 'production') {
		middleware = composeWithDevTools(middleware)
	}

	const store = createStore(rootReducer as any, initialState as any, middleware as any) as Store<RootState>

	if (module.hot) {
		module.hot.accept('app/reducers', () => {
			const nextReducer = require('app/reducers')
			store.replaceReducer(nextReducer)
		})
	}

	return store
}
