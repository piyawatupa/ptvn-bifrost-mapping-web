var winWidth;
var winHeight;
winWidth = $(window).width();
winHeight = $(window).height();

var init = {
    initFunctionReady: function() {
        init.loginAction();
        init.placeholderDropdown();
        init.dropdownCheckList();
        init.modal();
        init.tooltip();
        init.dropdownTag();
        init.dropdownPlaceholder();
    },

    initFunctionResize: function() {
        init.checkHeightSize();
    },

    checkHeightSize: function() {
        winWidth = $(window).width();
        winHeight = $(window).height();
    },

    /*checkMiddle: function() {
        var middle = $(window);
        var middleInner = $('.body-login .row>.col-6');
        var middleHeight = middle.height();
        middleInner.removeAttr('style').css('height', middleHeight + 'px');
    },*/

    loginAction: function() {
        const body = $('.body-login');
        const nav = $('.navbar');
        const container = $('.body-login>[class*=container-]');
        const buttton = $('.btn-outline');
        const sec = 400;

        $(buttton).click(function(e) {
            e.preventDefault;
            $(container).each(function() {
                $(this).closest(container).toggleClass('show hide').delay(sec).queue(function(){      
                    $(this).toggleClass('above').dequeue();
                });
            });
            $(this).closest(body).find(nav).delay(sec).queue(function(){ 
                nav.toggleClass('light dark').dequeue();
            });
        });
    },
    placeholderDropdown: function() {
        $('.dropdown, .lang').on('click', '.dropdown-item', function(){
            if($(this).children().hasClass('row')) {
                $(this).parent().parent().find('.dropdown-toggle').html('<img src="'+ $(this).find('img').attr("src") + '" class="icon-flags" alt="" />');
            } else if($(this).children().hasClass('dropdown-inline-icon') && !$(this).parent().parent().hasClass("dropdown-tag")) {
                $(this).parent().parent().find('.dropdown-toggle, .dropdown-text').html($(this).html());
                $(this).parent().parent().find('.dropdown-text').removeClass('filtered')
                $(this).closest('.dropdown').find('input').val('');
            } else if($(this).data("title")) {
                $(this).parent().parent().find('.dropdown-toggle').html('<span>'+ $(this).data("title") + '</span>');
            } else if($(this).parent().parent().hasClass("dropdown-tag")) {
                $(this).parent().parent().find('.dropdown-toggle').prepend('<span class="btn-tag tag-primary"><img src="../../assets/images/flags/flag-th.svg" class="icon-first">Thailand<i class="icon-cancel-circle close"><span class="path1"></span><span class="path2"></span></i></span>');
            } else {
                $(this).parent().parent().find('.dropdown-toggle').html('<span>'+ $(this).html() + '</span>');
            }
        });
        
    },
    dropdownCheckList: function() {
        $('.table-col-scroll .dropdown-menu').click(function(e) {
            e.stopPropagation();
        });
    },
    modal: function() {
        $(".modal").modal();
    },
    tooltip: function() {
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    },
    checkField() {
        if (document.forms['frm'].question.value === "") {
            alert("empty");
            return false;
        }
    },
    dropdownPlaceholder() {
        var dropdownInput = $(".dropdown input");

        dropdownInput.keyup(function () {
            $(this).parent().find('.dropdown-text .placeholder').removeClass('hide')
            $(this).parent().find('.dropdown-text').removeClass('filtered')
            if($(this).val() != '') {
                $(this).parent().find('.dropdown-text .placeholder').addClass('hide')
                $(this).parent().find('.dropdown-text').addClass('filtered')
            }

            $(this).dropdown('update');

            var value = this.value.toLowerCase().trim();
            $(this).next().children().show().filter(function() {
                return $(this).text().toLowerCase().trim().indexOf(value) == -1;
            }).hide();
        });
    },
    dropdownTag() {
        var dropdownInputTag = $(".dropdown-tag input");

        dropdownInputTag.keyup(function () {
            $(this).next('.placeholder').removeClass('hide')
            if($(this).val() != '') {
                $(this).next('.placeholder').addClass('hide')
            }

            $(this).parent().dropdown('update');

            var value = this.value.toLowerCase().trim();
            $(this).parent().next().children().show().filter(function() {
                return $(this).text().toLowerCase().trim().indexOf(value) == -1;
            }).hide();
        });

        dropdownInputTag.on('input', function () {
            var lng = $(this).val().length;
            $(this).width(lng * 10);
        });

        $('.dropdown-tag .dropdown-toggle').click(function(e) {
            if((e.target == this) || ($(this).children('.placeholder'))) {
                $.fn.setCursorPosition = function (pos) {
                    this.each(function (index, elem) {
                        if (elem.setSelectionRange) {
                            elem.setSelectionRange(pos, pos);
                        } else if (elem.createTextRange) {
                            var range = elem.createTextRange();
                            range.collapse(true);
                            range.moveEnd('character', pos);
                            range.moveStart('character', pos);
                            range.select();
                        }
                    });
                    return this;
                };
                $(this).find('input').focus().setCursorPosition(1000);
            }
        });

        $('.dropdown .btn-tag').click(function(e) {
            e.stopPropagation();
        });
    }
};
$(document).ready(init.initFunctionReady);
$(window).resize(init.initFunctionResize);