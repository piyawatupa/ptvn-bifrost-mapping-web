# pull official base image
FROM node:alpine

# set working directory
WORKDIR /usr/app

# install app dependencies
COPY package.json ./
RUN npm install --silent

# add app
COPY . ./

EXPOSE 3002

# start app
CMD ["npm", "start"]