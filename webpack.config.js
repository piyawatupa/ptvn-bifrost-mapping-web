var webpack = require("webpack");
var path = require("path");
var package = require("./package.json");
var env =
  process.env.NODE_ENV === undefined
    ? require("./environments/development.json")
    : require(`./environments/${process.env.NODE_ENV}.json`);
// variables
var isProduction =
  process.argv.indexOf("-p") >= 0 || process.env.NODE_ENV === "production";
var sourcePath = path.join(__dirname, "./src");
var outPath = path.join(__dirname, "./build");

// plugins
var HtmlWebpackPlugin = require("html-webpack-plugin");
var CleanWebpackPlugin = require("clean-webpack-plugin");

module.exports = {
  context: sourcePath,
  entry: {
    app: "./main.tsx"
  },

  output: {
    path: outPath,
    filename: isProduction ? "[contenthash].js" : "[hash].js",
    chunkFilename: isProduction ? "[name].[contenthash].js" : "[name].[hash].js"
  },
  target: "web",
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".jsx"],
    // Fix webpack's default behavior to not load packages with jsnext:main module
    // (jsnext:main directs not usually distributable es6 format, but es6 sources)
    mainFields: ["module", "browser", "main"],
    alias: {
      app: path.resolve(__dirname, "src/app/"),
      '@actions' : path.resolve(__dirname,"src/app/actions"),
      '@components' : path.resolve(__dirname,"src/app/components"),
      '@containers' : path.resolve(__dirname,"src/app/containers"),
      '@helper' : path.resolve(__dirname,"src/app/helper"),
      '@services' : path.resolve(__dirname,"src/app/services"),
      '@hook' : path.resolve(__dirname,"src/app/hook"),
      '@models' : path.resolve(__dirname,"src/app/models"),
      '@reducers' : path.resolve(__dirname,"src/app/reducers"),
      "react-dom": "@hot-loader/react-dom"
    }
  },
  module: {
    rules: [
      // .ts, .tsx
      {
        test: /\.tsx?$/,
        use: [
          !isProduction && {
            loader: "babel-loader",
            options: { plugins: ["react-hot-loader/babel"] }
          },
          "ts-loader"
        ].filter(Boolean)
      },
      // css
      {
        test: /\.(less)$/,
        use: [
          {
            loader: "style-loader" // creates style nodes from JS strings
          },
          {
            loader: "css-loader" // translates CSS into CommonJS
          },
          {
            loader: "less-loader" // compiles Less to CSS
          }
        ]
      },
      {
        test: /\.(css)$/,
        use: [
          {
            loader: "style-loader" // creates style nodes from JS strings
          },
          {
            loader: "css-loader" // translates CSS into CommonJS
          },
          {
            loader: "less-loader" // compiles Less to CSS
          }
        ]
      },
      // static assets
      { test: /\.html$/, use: "html-loader" },
      { test: /\.(a?png|svg)$/, use: "url-loader?limit=10000" },
      {
        test: /\.(jpe?g|gif|bmp|mp3|mp4|ogg|wav|eot|ttf|woff|woff2)$/,
        use: "file-loader"
      }
    ]
  },
  optimization: {
    splitChunks: {
      name: true,
      cacheGroups: {
        commons: {
          chunks: "initial",
          minChunks: 2
        },
        vendors: {
          test: /[\\/]node_modules[\\/]/,
          chunks: "all",
          filename: isProduction
            ? "vendor.[contenthash].js"
            : "vendor.[hash].js",
          priority: -10
        }
      }
    },
    runtimeChunk: true
  },

  plugins: [
    new webpack.EnvironmentPlugin({
      NODE_ENV: "development", // use 'development' unless process.env.NODE_ENV is defined
      GATEWAY_URL: env.GATEWAY_URL,
      DEBUG: false
    }),
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: "assets/index.html",
      minify: {
        minifyJS: true,
        minifyCSS: true,
        removeComments: true,
        useShortDoctype: true,
        collapseWhitespace: true,
        collapseInlineTagWhitespace: true
      },
      append: {
        head: `<script src="//cdn.polyfill.io/v3/polyfill.min.js"></script>`
      },
      meta: {
        title: package.name,
        description: package.description,
        keywords: Array.isArray(package.keywords)
          ? package.keywords.join(",")
          : undefined
      }
    })
  ],
  devServer: {
    contentBase: sourcePath,
    hot: true,
    inline: true,
    historyApiFallback: {
      disableDotRule: true
    },
    stats: "minimal",
    clientLogLevel: "warning"
  },
  // https://webpack.js.org/configuration/devtool/
  devtool: isProduction ? "hidden-source-map" : "cheap-module-eval-source-map",
  node: {
    // workaround for webpack-dev-server issue
    // https://github.com/webpack/webpack-dev-server/issues/60#issuecomment-103411179
    fs: "empty",
    net: "empty"
  }
};
